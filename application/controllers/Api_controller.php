<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
	}
	
	public function list_berita(){
		if (isset($_SERVER['HTTP_ORIGIN'])){
			header("Access-Control-Allow-Origin: *");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');
			header("Content-type:application/json");
		}

		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

			exit(0);
		}
		
		$query_list_berita = $this->m_berita->get_all_berita();
		if($query_list_berita){
			$list = array();
			foreach($query_list_berita->result() as $row){
				$data = array(
					'berita_id'			=>	$row->berita_id,
					'berita_judul'		=>	!empty($row->berita_judul) ? $row->berita_judul : "",
					'berita_isi'		=>	!empty($row->berita_isi) ? $row->berita_isi : "",
					'berita_gambar'		=>	!empty($row->berita_image) ? base_url()."assets/images/".$row->berita_image : "",
					'berita_tanggal'	=>	!empty($row->berita_tanggal) ? $row->berita_tanggal : ""
				);
				array_push($list, $data);
			}
			$json = array(
				'status_code' 	=> 200,
				'list'			=> $list
			);
		}else{
			$json = array(
				'status_code' 	=> 500,
				'message'		=> "Data not found."
			);
		}
		echo json_encode($json, true);
	}
}
