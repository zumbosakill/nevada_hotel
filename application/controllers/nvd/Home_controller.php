<?php
class Home_controller extends CI_Controller{
	function __construct(){
		parent::__construct();

	}
	function index(){
		$this->load->view('nvd/homePage/home');
	}

  function about(){
		$this->load->view('nvd/content/about');
	}

  function contact(){
		$this->load->view('nvd/content/contact');
	}

  function rooms(){
		$this->load->view('nvd/content/rooms');
	}

	function meeting(){
		$this->load->view('nvd/content/meeting');
	}

	function outlet(){
		$this->load->view('nvd/content/outlet');
	}

	function availableRoom(){
		$this->load->view('nvd/content/availableRoom');
	}


	// function view(){
	// 	$kode=$this->uri->segment(3);
	// 	$x['data']=$this->m_berita->get_berita_by_kode($kode);
	// 	$this->load->view('v_post_view',$x);
	// }

}
