<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Nevada Ketapang Hotel</title>
<!--
Holiday Template
http://www.templatemo.com/tm-475-holiday
-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700' rel='stylesheet' type='text/css'>
  <link href="<?php echo base_url(); ?>/assets/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/assets/css/flexslider.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/assets/css/templatemo-style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/assets/css/datepicker.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="icon" href="<?php echo base_url(); ?>/assets/img/logo/favicon.png" type="image/x-icon">
  </head>
  <!-- Header -->
  <body>
  <div class="tm-header">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-3 tm-site-name-container">
          <a href="<?php echo base_url(); ?>home" class="tm-site-name">Nevada Ketapang Hotel</a>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-9">
          <?php
            $url = $this->uri->segment(1);
          ?>
          <div class="mobile-menu-icon">
                <i class="fa fa-bars"></i>
          </div>
          <nav class="tm-nav">
          <ul>
            <li><a href="<?php echo base_url(); ?>home" class="<?php echo $url === 'home' ? 'active' : '' ;?>">Home</a></li>
            <li><a href="<?php echo base_url(); ?>rooms" class="<?php echo $url === 'rooms' ? 'active' : '' ;?>">Rooms</a></li>
            <li><a href="<?php echo base_url(); ?>outlet" class="<?php echo $url === 'outlet' ? 'active' : '' ;?>">Outlet</a></li>
            <li><a href="<?php echo base_url(); ?>meeting" class="<?php echo $url === 'meeting' ? 'active' : '' ;?>">Function</a></li>
            <!-- <li><a href="<?php echo base_url(); ?>about" class="<?php echo $url === 'about' ? 'active' : '' ;?>">About</a></li> -->
            <li><a href="<?php echo base_url(); ?>contact" class="<?php echo $url === 'contact' ? 'active' : '' ;?>">Contact</a></li>
          </ul>
        </nav>
        </div>
      </div>
    </div>
  </div>

  <section class="tm-banner">
    <!-- Flexslider -->
    <div class="flexslider flexslider-banner">
      <ul class="slides">
        <li>
          <div class="tm-banner-inner">
          <h1 class="tm-banner-title">Nevada <span class="tm-yellow-text">Ketapang</span> Hotel</h1>
          <p class="tm-banner-subtitle">Comfortable With Us</p>
          <a href="#more" class="tm-banner-link">Learn More</a>
        </div>
        <img src="<?php echo base_url(); ?>/assets/img/banner-1.jpg" alt="Image" />
        </li>
        <li>
          <div class="tm-banner-inner">
          <h1 class="tm-banner-title">Nevada <span class="tm-yellow-text">Ketapang</span> Hotel</h1>
          <p class="tm-banner-subtitle">Wonderful Destinations</p>
          <a href="#more" class="tm-banner-link">Learn More</a>
        </div>
          <img src="<?php echo base_url(); ?>/assets/img/banner-2.jpg" alt="Image" />
        </li>
        <li>
          <div class="tm-banner-inner">
          <h1 class="tm-banner-title">Nevada <span class="tm-yellow-text">Ketapang</span> Hotel</h1>
          <p class="tm-banner-subtitle">For</p>
          <p class="tm-banner-subtitle">Business Trip</p>
          <a href="#more" class="tm-banner-link">Learn More</a>
        </div>
          <img src="<?php echo base_url(); ?>/assets/img/banner-3.jpg" alt="Image" />
        </li>
      </ul>
    </div>
  </section>
