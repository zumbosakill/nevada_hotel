<div class="section-margin-top">
  <div class="row">
    <div class="tm-section-header">
      <div class="col-lg-3 col-md-3 col-sm-3"><hr></div>
      <div class="col-lg-6 col-md-6 col-sm-6"><h2 class="tm-section-title">Vacation Destination</h2></div>
      <div class="col-lg-3 col-md-3 col-sm-3"><hr></div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12">
      <div class="tm-home-box-2">
        <img src="<?php echo base_url(); ?>/assets/img/index-03.jpg" alt="image" class="img-responsive">
        <h3>Klenteng Tua</h3>
        <p class="tm-date">05 ‎Desember ‎2017</p>
        <div class="tm-home-box-2-container">
          <a class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
          <a class="tm-home-box-2-link"><span class="tm-home-box-2-description">Travel</span></a>
          <a class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12">
      <div class="tm-home-box-2">
          <img src="<?php echo base_url(); ?>/assets/img/index-04.jpg" alt="image" class="img-responsive">
        <h3>Rumah Betang</h3>
        <p class="tm-date">05 ‎Desember ‎2017</p>
        <div class="tm-home-box-2-container">
          <a class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
          <a class="tm-home-box-2-link"><span class="tm-home-box-2-description">Travel</span></a>
          <a class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12">
      <div class="tm-home-box-2">
          <img src="<?php echo base_url(); ?>/assets/img/index-05.jpg" alt="image" class="img-responsive">
        <h3>Rumah Adat Melayu</h3>
        <p class="tm-date">05 ‎Desember ‎2017</p>
        <div class="tm-home-box-2-container">
          <a class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
          <a class="tm-home-box-2-link"><span class="tm-home-box-2-description">Travel</span></a>
          <a class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12">
      <div class="tm-home-box-2 tm-home-box-2-right">
          <img src="<?php echo base_url(); ?>/assets/img/index-06.jpg" alt="image" class="img-responsive">
        <h3>Hutan Kota</h3>
        <p class="tm-date">05 ‎Desember ‎2017</p>
        <div class="tm-home-box-2-container">
          <a class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
          <a class="tm-home-box-2-link"><span class="tm-home-box-2-description">Travel</span></a>
          <a class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <p class="home-description">Enjoy the convenience of a break during a journey you are with us nevada hotel ketapang</p>
    </div>
  </div>
</div>
