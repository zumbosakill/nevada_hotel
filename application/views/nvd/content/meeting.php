<?php $this->load->view("header.php"); ?>



	<!-- gray bg -->
	<section class="container tm-home-section-1" id="more">


		<div class="section-margin-top">
			<div class="row">
				<div class="tm-section-header">
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>
					<div class="col-lg-6 col-md-6 col-sm-6"><h2 class="tm-section-title">Our Meeting Rooms</h2></div>
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="tm-tours-box-1">
						<img src="<?php echo base_url(); ?>assets/img/fucntion/DSCF0850.jpg" alt="image" class="img-responsive">
						<div class="tm-tours-box-1-info">
							<div class="tm-tours-box-1-info-left">
								<p class="text-uppercase margin-bottom-20">Arizona</p>
								<p class="gray-text">28 Agustus 2018</p>
							</div>
							<div class="tm-tours-box-1-info-right">
								<p class="gray-text tours-1-description">For 15pax meeting room see detail for information</p>
							</div>
						</div>
						<div class="tm-tours-box-1-link">
							<div class="tm-tours-box-1-link-left">
								Confortable With Us
							</div>
							<a href="#" class="tm-tours-box-1-link-right">
								Reserve
							</a>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="tm-tours-box-1">
						<img src="<?php echo base_url(); ?>assets/img/fucntion/BALL ROOM 4.jpg" alt="image" class="img-responsive">
						<div class="tm-tours-box-1-info">
							<div class="tm-tours-box-1-info-left">
								<p class="text-uppercase margin-bottom-20">Atlanta</p>
								<p class="gray-text">26 March 2084</p>
							</div>
							<div class="tm-tours-box-1-info-right">
								<p class="gray-text tours-1-description">Standar room with anything you want.s</p>
							</div>
						</div>
						<div class="tm-tours-box-1-link">
							<div class="tm-tours-box-1-link-left">
								Confortable With Us
							</div>
							<a href="#" class="tm-tours-box-1-link-right">
								Reserve
							</a>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="tm-tours-box-1">
						<img src="<?php echo base_url(); ?>assets/img/fucntion/BALL ROOM 5.jpg" alt="image" class="img-responsive">
						<div class="tm-tours-box-1-info">
							<div class="tm-tours-box-1-info-left">
								<p class="text-uppercase margin-bottom-20">Michigan</p>
								<p class="gray-text">24 March 2084</p>
							</div>
							<div class="tm-tours-box-1-info-right">
								<p class="gray-text tours-1-description">Standar room with anything you want.</p>
							</div>
						</div>
						<div class="tm-tours-box-1-link">
							<div class="tm-tours-box-1-link-left">
								Confortable With Us
							</div>
							<a href="#" class="tm-tours-box-1-link-right">
								Reserve
							</a>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="tm-tours-box-1">
						<img src="<?php echo base_url(); ?>assets/img/fucntion/BALL ROOM 2.jpg" alt="image" class="img-responsive">
						<div class="tm-tours-box-1-info">
							<div class="tm-tours-box-1-info-left">
								<p class="text-uppercase margin-bottom-20">Nevada</p>
								<p class="gray-text">22 March 2084</p>
							</div>
							<div class="tm-tours-box-1-info-right">
								<p class="gray-text tours-1-description">Standar room with anything you want.</p>
							</div>
						</div>
						<div class="tm-tours-box-1-link">
							<div class="tm-tours-box-1-link-left">
								Confortable With Us
							</div>
							<a href="#" class="tm-tours-box-1-link-right">
								Reserve
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- white bg -->
	<section class="tm-white-bg section-padding-bottom">
		<div class="container">
			<?php $this->load->view("nvd/content/destination.php"); ?>
		</div>
	</section>
<?php $this->load->view("footer.php"); ?>
