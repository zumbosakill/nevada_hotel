<section class="tm-white-bg section-padding-bottom">
  <div class="container">
    <div class="row">
      <div class="tm-section-header section-margin-top">
        <div class="col-lg-4 col-md-3 col-sm-3"><hr></div>
        <div class="col-lg-4 col-md-6 col-sm-6"><h2 class="tm-section-title">Popular Packages</h2></div>
        <div class="col-lg-4 col-md-3 col-sm-3"><hr></div>
      </div>
      <div class="col-lg-6">
        <div class="tm-home-box-3">
          <div class="tm-home-box-3-img-container">
            <img src="<?php echo base_url(); ?>/assets/img/index-07.jpg" alt="image" class="img-responsive">
          </div>
          <div class="tm-home-box-3-info">
            <p class="tm-home-box-3-description">Honey Moon Packet</p>
                <div class="tm-home-box-2-container">
            <a class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
            <a class="tm-home-box-2-link"><span class="tm-home-box-2-description box-3">Travel</span></a>
            <a class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
          </div>
          </div>
        </div>
         </div>
         <div class="col-lg-6">
            <div class="tm-home-box-3">
          <div class="tm-home-box-3-img-container">
            <img src="<?php echo base_url(); ?>/assets/img/index-08.jpg" alt="image" class="img-responsive">
          </div>
          <div class="tm-home-box-3-info">
            <p class="tm-home-box-3-description">Weeding Packet</p>
                <div class="tm-home-box-2-container">
            <a class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
            <a class="tm-home-box-2-link"><span class="tm-home-box-2-description box-3">Travel</span></a>
            <a class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
          </div>
          </div>
        </div>
      </div>
      <!--
      <div class="col-lg-6">
          <div class="tm-home-box-3">
          <div class="tm-home-box-3-img-container">
            <img src="img/index-09.jpg" alt="image" class="img-responsive">
          </div>
          <div class="tm-home-box-3-info">
            <p class="tm-home-box-3-description">Proin gravida nibhvell velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum</p>
                <div class="tm-home-box-2-container">
            <a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
            <a href="#" class="tm-home-box-2-link"><span class="tm-home-box-2-description box-3">Travel</span></a>
            <a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
          </div>
          </div>
        </div>
        </div>
        <div class="col-lg-6">
            <div class="tm-home-box-3">
          <div class="tm-home-box-3-img-container">
            <img src="img/index-10.jpg" alt="image" class="img-responsive">
          </div>
          <div class="tm-home-box-3-info">
            <p class="tm-home-box-3-description">Proin gravida nibhvell velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum</p>
                <div class="tm-home-box-2-container">
            <a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
            <a href="#" class="tm-home-box-2-link"><span class="tm-home-box-2-description box-3">Travel</span></a>
            <a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
          </div>
          </div>
        </div>
      </div> -->
    </div>
  </div>
</section>
