<?php $this->load->view("header.php"); ?>
<!-- Banner -->


<!-- gray bg -->
<section class="container tm-home-section-1" id="more">
  <div class="row">
    <div class="col-lg-4 col-md-4 col-sm-6">
      <!-- Nav tabs -->
      <div class="tm-home-box-1">
        <ul class="nav nav-tabs tm-white-bg" role="tablist" id="hotelCarTabs">
            <li role="presentation" class="active">
              <a href="#hotel" aria-controls="hotel" role="tab" data-toggle="tab">Hotel</a>
            </li>
            <li role="presentation">
              <a href="#car" aria-controls="car" role="tab" data-toggle="tab">Vehicle Rental</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active tm-white-bg" id="hotel">
              <div class="tm-search-box effect2">
              <form action="../HotelB/checkroom.php" method="post" onSubmit="return validateForm(this);" class="hotel-search-form">
                <div class="tm-form-inner">

                        <div class="form-group">
                            <div class='input-group date-time' id='datetimepicker3'>
                                <input autocomplete="off" type='text' class="form-control" name="checkin" id="checkin" placeholder="Check In" />
                              <!--  <span class="input-group-addon">

                              </span> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <div class='input-group date-time' id='datetimepicker3'>
                                <input autocomplete="off" type='text' class="form-control" name="checkout" id="checkout" placeholder="Check " />
                              <!--  <span class="input-group-addon">

                              </span> -->
                            </div>
                        </div>
                        <div class="form-group">
                          <select class="form-control" name="totaladults" id="totaladults">
                            <option value="0">-- Adult -- </option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                          </select>
                        </div>
                        <div class="form-group margin-bottom-0">
                          <select class="form-control" name="totalchildrens" id="totalchildrens">
                            <option value="0">-- Child -- </option>
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4p">4+</option>
                          </select>
                        </div>
                </div>
                      <div class="form-group tm-yellow-gradient-bg text-center">
                        <button type="submit" name="submit" class="tm-yellow-btn">Check Availabelity</button>
                      </div>
              </form>
            </div>
            </div>
            <div role="tabpanel" class="tab-pane fade tm-white-bg" id="car">
            <div class="tm-search-box effect2">
              <form action="#" method="post" class="hotel-search-form">
                <div class="tm-form-inner">
                  <div class="form-group">
                           <select class="form-control">
                            <option value="">-- Select Model -- </option>
                            <option value="shangrila">BMW</option>
                      <option value="chatrium">Mercedes-Benz</option>
                      <option value="fourseasons">Toyota</option>
                      <option value="hilton">Honda</option>
                    </select>
                        </div>
                        <div class="form-group">
                            <div class='input-group date-time' id='datetimepicker3'>
                                <input type='text' class="form-control" placeholder="Pickup Date" />
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class='input-group date-time' id='datetimepicker4'>
                                <input type='text' class="form-control" placeholder="Return Date" />
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                           <select class="form-control">
                            <option value="">-- Options -- </option>
                            <option value="">Child Seat</option>
                      <option value="">GPS Navigator</option>
                      <option value="">Insurance</option>
                    </select>
                        </div>
                </div>
                      <div class="form-group tm-yellow-gradient-bg text-center">
                        <button type="submit" name="submit" class="tm-yellow-btn">Check Now</button>
                      </div>
              </form>
            </div>
            </div>
        </div>
      </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6">
      <div class="tm-home-box-1 tm-home-box-1-2 tm-home-box-1-center">
        <img src="<?php echo base_url(); ?>/assets/img/index-01.jpg" alt="image" class="img-responsive">
        <a href="#">
          <div class="tm-green-gradient-bg tm-city-price-container">
            <span>Junior Suite</span>
            <span>Rp 1.200.00</span>
          </div>
        </a>
      </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6">
      <div class="tm-home-box-1 tm-home-box-1-2 tm-home-box-1-right">
        <img src="<?php echo base_url(); ?>/assets/img/index-02.jpg" alt="image" class="img-responsive">
        <a href="#">
          <div class="tm-red-gradient-bg tm-city-price-container">
            <span>Deluxe</span>
            <span>Rp 800.000</span>
          </div>
        </a>
      </div>
    </div>
  </div>

<?php $this->load->view("nvd/content/destination.php"); ?>
</section>

<?php $this->load->view("nvd/content/package.php"); ?>

<?php $this->load->view("footer.php"); ?>
<script>
	function validateForm(form) {
		var a = form.checkin.value;
		var b = form.checkout.value;
		var c = form.totaladults.value;
		var d = form.totalchildrens.value;
			if(a == null || b == null || a == "" || b == "")
			{
			 alert("Please choose date");
			 return false;
			}
			if(c == 0)
			{
			 	if(d == 0)
				{
				 alert("Please choose no. of guest");
				 return false;
				}
			}
			if(d == 0)
			{
			 	if(c == 0)
				{
				 alert("Please choose no. of guest");
				 return false;
				}
			}

	}
</script>

<script>
  $(document).ready(function() {
    $("#checkout").datepicker();
    $("#checkin").datepicker({
    minDate : new Date(),
    onSelect: function (dateText, inst) {
        var date = $.datepicker.parseDate($.datepicker._defaults.dateFormat, dateText);
        $("#checkout").datepicker("option", "minDate", date);
    }
    });
  });
</script>
